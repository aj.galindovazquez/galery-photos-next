import styled, { keyframes } from 'styled-components'

const efect = keyframes`
    0%,
    100% {
      opacity: 1;
    }
  
    50% {
      opacity: 0.5;
    }
`
export const LdsGrid = styled.div`
    animation: ${efect};
    display: inline-block;
    position: relative;
    width: 64px;
    height: 64px;
    &:nth-child(1){
        top: 6px;
        left: 6px;
        animation-delay: 0s;
    }
    &:nth-child(2){
        top: 6px;
        left: 26px;
        animation-delay: -0.4s;
    }
    &:nth-child(3){
        top: 6px;
        left: 45px;
        animation-delay: -0.8s;
    }
    &:nth-child(4){
        top: 26px;
        left: 6px;
        animation-delay: -0.4s;
    }
    &:nth-child(5){
        top: 26px;
        left: 26px;
        animation-delay: -0.8s;
    }
    &:nth-child(6){
        top: 26px;
        left: 45px;
        animation-delay: -1.2s;
    }
    &:nth-child(7){
        top: 45px;
        left: 6px;
        animation-delay: -0.8s;
    }
    &:nth-child(8){
        top: 45px;
        left: 26px;
        animation-delay: -1.2s;
    }
    &:nth-child(9){
        top: 45px;
        left: 45px;
        animation-delay: -1.6s;
    }

`
