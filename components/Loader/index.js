import React from 'react'
import { LdsGrid } from './styles'

export const Loader = () => {
  return (
    <LdsGrid>
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
    </LdsGrid>
  )
}
