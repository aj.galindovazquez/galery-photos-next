import React from 'react'
import Card from 'react-bootstrap/Card'
import { AiFillLike, AiFillStar, AiOutlineCloudDownload } from 'react-icons/ai'
export const PhotoView = ({ image}) => {
  /* console.log(image, loading)
  if (loading) {
    return (
      <img src='./photoCardImageDefault.jpg' />
    )
  } */
  
  if (image) {
    const photo=image[0]
    return (
      <div className='row m-5'>
        <div className='col-md-4 col-sm-12 p-3'>
          <Card className='text-center'>
            <Card.Header>vistas {photo.views}</Card.Header>
            <Card.Body>
              <Card.Title className='text-capitalize'>{photo.tags}</Card.Title>
              <Card.Text>
                <p>{photo.likes} <AiFillLike className='ml-2' /></p>
                <p>{photo.favorites} <AiFillStar className='ml-2' /></p>
                <p>{photo.downloads} <AiOutlineCloudDownload className='ml-2' />
                </p>

              </Card.Text>
            </Card.Body>
            <Card.Footer className='text-muted'>user: {photo.user}</Card.Footer>
          </Card>
        </div>
        <div className='col-md-8 col-sm-12 d-flex justify-content-center'>
          <img src={photo.webformatURL} class='img-fluid' alt={photo.tags} />
        </div>
      </div>
    )
  }

  return (
    <h2>casi listo</h2>
  )
}
