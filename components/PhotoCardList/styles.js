import styled from 'styled-components'

export const PhotoCardListE = styled.div`
    margin: 40px;
`

export const PhotoCardListDescription = styled.p`
    color: ${props => props.theme.color.midGrey};
`
