import React from 'react'
import { PhotoCard } from '../PhotoCard'
import Alert from 'react-bootstrap/Alert'
import { PhotoCardListE, PhotoCardListDescription } from './styles'

export const PhotoCardList = (props) => {
  const images = props.images ? props.images : ['img1', 'img2', 'img3']
  if (props.total === 0) {
    return (
      <Alert variant='danger' className='container-alert'>
        No se encontrarón resultados
      </Alert>
    )
  }
  if (props.loading) {
    return (
      <PhotoCardListE>
        <PhotoCardListDescription>{props.total} resultados</PhotoCardListDescription>
        <div className='row'>
          {images.map((image) => (
            <div className='col p-0 m-1'> <PhotoCard key={image} /> </div>
          ))}
        </div>
      </PhotoCardListE>
    )
  }
  return (
    <PhotoCardListE>
      <PhotoCardListDescription>{props.total} resultados</PhotoCardListDescription>
      <div className='row'>
        {images.map((image) => (
          <div className='col p-0 m-1'> <PhotoCard key={image.id} srcPhoto={image.previewURL} {...image} /> </div>
        ))}
      </div>
    </PhotoCardListE>
  )
}
