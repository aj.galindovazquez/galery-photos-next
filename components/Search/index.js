import React from 'react'
import Nav from 'react-bootstrap/Nav'
import Form from 'react-bootstrap/Form'
import PropTypes from 'prop-types'
/**
 * Search: It helps us filter the images by: category, order and orientation.
 */
const SearchComponet = ({ searchValues, onChange, onClick }) => {
  const categories = [
    { id: 'all', title: 'Todos' },
    { id: 'animals', title: 'Animales' },
    { id: 'buildings', title: 'Arquitectura / Edificios' },
    { id: 'fashion', title: 'Belleza / Moda' },
    { id: 'science', title: 'Ciencia / Tecnología' },
    { id: 'food', title: 'Comida / Bebidas' },
    { id: 'computer', title: 'Computador / Comunicación' },
    { id: 'sports', title: 'Deportes' },
    { id: 'education', title: 'Educación' },
    { id: 'feelings', title: 'Emociones' },
    { id: 'backgrounds', title: 'Fondos/Texturas' },
    { id: 'people', title: 'Gente' },
    { id: 'industry', title: 'Industria / Manofactura' },
    { id: 'places', title: 'Lugares / Monumentos' },
    { id: 'music', title: 'Música' },
    { id: 'nature', title: 'Naturaleza / Paisajes' },
    { id: 'business', title: 'Negocios / Finanzas' },
    { id: 'religion', title: 'Religiones' },
    { id: 'health', title: 'Salud / Médica' },
    { id: 'transportation', title: 'Transporte / Tráfico' },
    { id: 'travel', title: 'Viajes / Vacaciones' }
  ]

  return (
    <Nav variant='pills'>
      <Nav.Item className='m-2' key='nav-1'>
        <Form.Control
          as='select'
          onChange={onChange}
          value={searchValues.order}
          name='order'
        >
          <option value='popular' key='popular'>Popular</option>
          <option value='latest' key='latest'>Antiguas</option>
        </Form.Control>
      </Nav.Item>
      <Nav.Item className='m-2' key='nav-2'>
        <Form.Control
          as='select'
          onChange={onChange}
          value={searchValues.orientation}
          name='orientation'
        >
          <option value='all' key='all'>Cualquier orientación</option>
          <option value='horizontal' key='horizontal'>Horizontal</option>
          <option value='vertical' key='vertical'>Vertical</option>
        </Form.Control>
      </Nav.Item>
      <Nav.Item className='m-2' key='nav-3'>
        <Form.Control
          as='select'
          onChange={onChange}
          value={searchValues.category}
          name='category'
        >
          {categories.map((category) => (<option value={category.id} key={category.id}>{category.title}</option>))}

        </Form.Control>
      </Nav.Item>
    </Nav>
  )
}

SearchComponet.propTypes = {
  order: PropTypes.string,
  orientation: PropTypes.string,
  category: PropTypes.string
}

export const Search = React.memo(SearchComponet)
