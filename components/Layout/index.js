import React from 'react'
import { Helmet } from 'react-helmet'
import { Footer } from '../Footer'
import Head from 'next/head'
export const Layout = ({ children, title, subtitle, previewPhoto, dataphoto }) => {
  if (dataphoto) {
    const photo = dataphoto[0]
    subtitle = photo.user
    title = photo.tags
    previewPhoto = photo.webformatURL
  }
  return (
    <>
      {/*  <Helmet>
        {title && <title>{title}</title>}
        {subtitle && <meta name='description' content={subtitle} />}
      </Helmet> */}
      <Head>
        <title>{title}</title>
        <meta name='description' content={subtitle} />
        <meta property='og:image' content={previewPhoto} />

      </Head>
      {children}
      <Footer />
    </>
  )
}
