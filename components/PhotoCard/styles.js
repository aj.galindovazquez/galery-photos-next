import styled from 'styled-components'
import Card from 'react-bootstrap/Card'
import { fadeIn } from '../../styles/animation'

export const CardPhoto = styled(Card)`
    height:250px;
    min-width:150px;
`
export const ImgIcon = styled.img`
    width: auto;
    height: 13px;
`
export const CardImg = styled(Card.Img)`
    ${fadeIn({ time: '3s' })}
    width: auto;
    display: block;
    height: 250px;
    cursor: pointer;
`

export const CardTitle = styled(Card.Title)`
    text-transform: capitalize;
    visibility: hidden;
`
export const CardText = styled(Card.Text)`
    visibility: hidden; 
`
export const CardImgOverlay = styled(Card.ImgOverlay)`
    &:hover{
        background-color: rgba(41, 41, 41, 0.376);
    }
    &:hover ${CardTitle}{
        visibility:visible
    }
    &:hover ${CardText}{
        visibility:visible
    }
`
