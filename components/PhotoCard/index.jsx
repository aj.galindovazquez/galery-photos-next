import React from 'react'
import { useNearScreen } from '../../hooks/useNearScreen'
import { CardPhoto, ImgIcon, CardImg, CardImgOverlay, CardTitle, CardText } from './styles'
import Link from 'next/link'

export const PhotoCard = ({ srcPhoto = './photoCardImageDefault.jpg', tags = 'Perro', favorites = '0', comments = '0', likes = '0', id }) => {
  // const [show, element] = useNearScreen()
  const show = true
  return (
    <CardPhoto className='bg-ligth'>
      {show &&
        <>
          <CardImg id='CardImg' src={srcPhoto} alt='Card image' />
          <Link href='/photo/[id]' as={`/photo/${id}/`} passHref>
            <CardImgOverlay className='text-white'>
              <CardTitle id='CardTitle'>{tags}</CardTitle>
              <CardText>
                <ImgIcon src='./like-white.png' alt='like' className='ml-3' />{likes}
                <ImgIcon src='./msn-white.png' alt='like' className='ml-3' /> {comments}
                <ImgIcon src='./star-white.png' alt='like' className='ml-3' />{favorites}
              </CardText>
            </CardImgOverlay>
          </Link>
        </>}
    </CardPhoto>
  )
}
