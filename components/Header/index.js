import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'

export const Header = ({ onChange, formValues, onClick }) => {
  /// Inicializando el estado
  return (
    <Navbar bg='light' expand='lg'>
      <Navbar.Brand href='#home'>
        Moments
      </Navbar.Brand>
      <Navbar.Toggle aria-controls='basic-navbar-nav' />
      <Navbar.Collapse id='basic-navbar-nav'>
        <Nav className='mr-auto' />
        <Form inline>
          <Form.Control
            as='select'
            onChange={onChange}
            value={formValues.image_type}
            name='image_type'
          >
            <option value='all'>Todo</option>
            <option value='photo'>Foto</option>
            <option value='vector'>Vector</option>
            <option value='ilustration'>Ilustración</option>
          </Form.Control>
          <FormControl
            onChange={onChange}
            type='text'
            placeholder='buscar'
            className='mr-sm-2 ounded-pill'
            name='q'
            value={formValues.q}
          />
          <Button
            variant='outline-success rounded-pill'
            onClick={onClick}
          >
            Buscar
          </Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  )
}
