import React from 'react'
import { Div, Label } from './styles'

export const Footer = () => (
  <Div>
    <Label id='phrase'>“Alégrate porque todo lugar es aquí y todo momento es ahora”
    </Label>
  </Div>
)
