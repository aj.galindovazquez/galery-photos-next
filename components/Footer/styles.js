import styled from 'styled-components'

export const Div = styled.div`
    background: ${props => props.theme.color.primary};
    border-top: 3px solid ${props => props.theme.color.secondary};
    bottom: 0;
    margin-top: 5px;
    margin-bottom: 5px;
    position: fixed;
    text-align: center;
    width: 100%;
    z-index: 2000;
`

export const Label = styled.label`
    @import url('https://fonts.googleapis.com/css2?family=Lato&family=Sansita+Swashed:wght@300;400;500;600;700;800;900&display=swap');
    color: ${props => props.theme.color.lightGrey};
    font-family: 'Sansita Swashed', cursive;
    font-weight: 300;
    &:hover {
        color: ${props => props.theme.color.midGrey};
    }
`
