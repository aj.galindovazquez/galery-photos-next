import { useState, useEffect } from 'react'



export function useGetData (endpoind, addData) {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState([false])
  const [error, setError] = useState('')

  function getData (busqueda, newPage) {
    setLoading(true)
    window.fetch(`${process.env.BASE_URL_APY}${busqueda}`)
      .then(res => res.json())
      .then(response => {
        setLoading(false)
        if (newPage) {
          const newData = data
          newData.hits = newData.hits.concat(response.hits)
          setData(newData)
        } else {
          setData(response)
        }
      })
      .catch(error => {
        console.log('Error', error)
        setError('No podemos conectarnos con el servidor')
        setData([])
        setLoading(false)
      })
  }

  useEffect(function () {
    getData(endpoind, addData)
  }, [endpoind])

  return [data, loading, error]
}
