import React from 'react'
import Theme from '../styles/Theme'
import { ThemeProvider } from 'styled-components'

const ProviderMock = props => (
  <ThemeProvider theme={Theme}>
    {props.children}
  </ThemeProvider>
)

export default ProviderMock