const PhotoListMock = {
  total: 283,
  totalHits: 283,
  hits: [
    {
      id: 410266,
      pageURL: 'https://pixabay.com/es/photos/la-construcci%C3%B3n-de-arquitectura-410266/',
      type: 'photo',
      tags: 'la construcción de, arquitectura, ciudad',
      previewURL: 'https://cdn.pixabay.com/photo/2014/08/05/09/37/building-410266_150.jpg',
      previewWidth: 98,
      previewHeight: 150,
      webformatURL: 'https://pixabay.com/get/52e1d5414c54b10ff3d8992cc620317f1437dbec4e507749762678d79e4cc2_640.jpg',
      webformatWidth: 418,
      webformatHeight: 640,
      largeImageURL: 'https://pixabay.com/get/52e1d5414c54b108f5d0846096293f78143ed6e05a4c704f752c72d79444c45f_1280.jpg',
      imageWidth: 3839,
      imageHeight: 5870,
      imageSize: 4085157,
      views: 29012,
      downloads: 14017,
      favorites: 96,
      likes: 89,
      comments: 28,
      user_id: 369698,
      user: 'ssalae',
      userImageURL: 'https://cdn.pixabay.com/user/2015/08/10/03-46-48-156_250x250.jpg'
    },
    {
      id: 4119806,
      pageURL: 'https://pixabay.com/es/photos/myeongdong-se%C3%BAl-corea-del-sur-4119806/',
      type: 'photo',
      tags: 'myeongdong, seúl, corea del sur',
      previewURL: 'https://cdn.pixabay.com/photo/2019/04/11/13/38/myeongdong-4119806_150.jpg',
      previewWidth: 120,
      previewHeight: 150,
      webformatURL: 'https://pixabay.com/get/52e1d44a4252aa14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 512,
      webformatHeight: 640,
      largeImageURL: 'https://pixabay.com/get/52e1d44a4252aa14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 3072,
      imageHeight: 3840,
      imageSize: 3941497,
      views: 29930,
      downloads: 22864,
      favorites: 82,
      likes: 87,
      comments: 20,
      user_id: 12152046,
      user: 'Seoulinspired',
      userImageURL: 'https://cdn.pixabay.com/user/2019/04/11/14-01-18-640_250x250.jpg'
    },
    {
      id: 5475279,
      pageURL: 'https://pixabay.com/es/photos/barra-restaurante-escudo-5475279/',
      type: 'photo',
      tags: 'barra, restaurante, escudo',
      previewURL: 'https://cdn.pixabay.com/photo/2020/08/09/11/30/bar-5475279_150.jpg',
      previewWidth: 100,
      previewHeight: 150,
      webformatURL: 'https://pixabay.com/get/53e4d2464855a514f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 427,
      webformatHeight: 640,
      largeImageURL: 'https://pixabay.com/get/53e4d2464855a514f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 4000,
      imageHeight: 6000,
      imageSize: 6685596,
      views: 29654,
      downloads: 23289,
      favorites: 52,
      likes: 49,
      comments: 7,
      user_id: 13458823,
      user: 'viarami',
      userImageURL: 'https://cdn.pixabay.com/user/2019/08/29/18-08-11-822_250x250.jpg'
    },
    {
      id: 4823909,
      pageURL: 'https://pixabay.com/es/photos/metro-estaci%C3%B3n-de-metro-4823909/',
      type: 'photo',
      tags: 'metro, estación de metro, estación de tren',
      previewURL: 'https://cdn.pixabay.com/photo/2020/02/06/11/32/subway-4823909_150.jpg',
      previewWidth: 150,
      previewHeight: 97,
      webformatURL: 'https://pixabay.com/get/52e8d7404352a514f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 417,
      largeImageURL: 'https://pixabay.com/get/52e8d7404352a514f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 3000,
      imageHeight: 1956,
      imageSize: 1175455,
      views: 6964,
      downloads: 4531,
      favorites: 17,
      likes: 21,
      comments: 5,
      user_id: 8603285,
      user: 'asd14235714',
      userImageURL: ''
    },
    {
      id: 2968290,
      pageURL: 'https://pixabay.com/es/photos/se%C3%BAl-corea-del-sur-lotte-torre-2968290/',
      type: 'photo',
      tags: 'seúl, corea del sur, lotte',
      previewURL: 'https://cdn.pixabay.com/photo/2017/11/21/14/16/seoul-2968290_150.jpg',
      previewWidth: 150,
      previewHeight: 84,
      webformatURL: 'https://pixabay.com/get/54e9d34b485bac14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 360,
      largeImageURL: 'https://pixabay.com/get/54e9d34b485bac14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 3412,
      imageHeight: 1920,
      imageSize: 1479939,
      views: 17150,
      downloads: 8182,
      favorites: 71,
      likes: 76,
      comments: 12,
      user_id: 7012210,
      user: 'manuzoli',
      userImageURL: 'https://cdn.pixabay.com/user/2019/08/05/07-58-50-825_250x250.jpeg'
    },
    {
      id: 786573,
      pageURL: 'https://pixabay.com/es/photos/asia-corea-del-sur-la-primavera-786573/',
      type: 'photo',
      tags: 'asia, corea del sur, la',
      previewURL: 'https://cdn.pixabay.com/photo/2015/05/27/12/32/asia-786573_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/51e8d3464d51b10ff3d8992cc620317f1437dbec4e507749762678d79e4cc2_640.jpg',
      webformatWidth: 640,
      webformatHeight: 425,
      largeImageURL: 'https://pixabay.com/get/51e8d3464d51b108f5d0846096293f78143ed6e05a4c704f752c72d79444c45f_1280.jpg',
      imageWidth: 3008,
      imageHeight: 2000,
      imageSize: 1536775,
      views: 11468,
      downloads: 6072,
      favorites: 30,
      likes: 46,
      comments: 12,
      user_id: 211600,
      user: 'tampigns',
      userImageURL: 'https://cdn.pixabay.com/user/2015/04/16/19-49-45-34_250x250.jpg'
    },
    {
      id: 2599123,
      pageURL: 'https://pixabay.com/es/photos/metro-rep%C3%BAblica-de-corea-2599123/',
      type: 'photo',
      tags: 'metro, república de corea, metro de corea del sur',
      previewURL: 'https://cdn.pixabay.com/photo/2017/08/07/03/06/subway-2599123_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/54e5dc4a4b50af14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/54e5dc4a4b50af14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 5184,
      imageHeight: 3456,
      imageSize: 3901510,
      views: 8607,
      downloads: 4151,
      favorites: 54,
      likes: 42,
      comments: 1,
      user_id: 2293582,
      user: 'zheng2088',
      userImageURL: 'https://cdn.pixabay.com/user/2017/08/07/02-44-30-81_250x250.jpg'
    },
    {
      id: 1797828,
      pageURL: 'https://pixabay.com/es/photos/corea-del-sur-monta%C3%B1as-cielo-nubes-1797828/',
      type: 'photo',
      tags: 'corea del sur, montañas, cielo',
      previewURL: 'https://cdn.pixabay.com/photo/2016/11/04/13/52/south-korea-1797828_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/57e7dc444250a414f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/57e7dc444250a414f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 2201,
      imageHeight: 1467,
      imageSize: 1113877,
      views: 8798,
      downloads: 3484,
      favorites: 68,
      likes: 61,
      comments: 4,
      user_id: 12019,
      user: '12019',
      userImageURL: ''
    },
    {
      id: 2599115,
      pageURL: 'https://pixabay.com/es/photos/metro-rep%C3%BAblica-de-corea-2599115/',
      type: 'photo',
      tags: 'metro, república de corea, metro de corea del sur',
      previewURL: 'https://cdn.pixabay.com/photo/2017/08/07/03/05/subway-2599115_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/54e5dc4a4b53a914f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/54e5dc4a4b53a914f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 5184,
      imageHeight: 3456,
      imageSize: 3174909,
      views: 8028,
      downloads: 3830,
      favorites: 48,
      likes: 36,
      comments: 4,
      user_id: 2293582,
      user: 'zheng2088',
      userImageURL: 'https://cdn.pixabay.com/user/2017/08/07/02-44-30-81_250x250.jpg'
    },
    {
      id: 2832422,
      pageURL: 'https://pixabay.com/es/photos/corea-incheon-ganghwado-paisaje-2832422/',
      type: 'photo',
      tags: 'corea, incheon, ganghwado',
      previewURL: 'https://cdn.pixabay.com/photo/2017/10/09/04/27/korea-2832422_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/54e8d6414e50ae14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/54e8d6414e50ae14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 4500,
      imageHeight: 3000,
      imageSize: 3319858,
      views: 11774,
      downloads: 4961,
      favorites: 47,
      likes: 68,
      comments: 11,
      user_id: 6537541,
      user: 'dldusdn',
      userImageURL: ''
    },
    {
      id: 2839691,
      pageURL: 'https://pixabay.com/es/photos/hanok-namsan-se%C3%BAl-2839691/',
      type: 'photo',
      tags: 'hanok, namsan, seúl',
      previewURL: 'https://cdn.pixabay.com/photo/2017/10/11/02/13/hanok-2839691_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/54e8d64a4c5bad14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 423,
      largeImageURL: 'https://pixabay.com/get/54e8d64a4c5bad14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 4928,
      imageHeight: 3264,
      imageSize: 3581883,
      views: 11174,
      downloads: 4842,
      favorites: 47,
      likes: 46,
      comments: 2,
      user_id: 1080134,
      user: 'Hong_Kim',
      userImageURL: 'https://cdn.pixabay.com/user/2017/10/09/06-12-20-411_250x250.jpg'
    },
    {
      id: 786592,
      pageURL: 'https://pixabay.com/es/photos/changdeokgung-palacio-jard%C3%ADn-786592/',
      type: 'photo',
      tags: 'changdeokgung, palacio, jardín',
      previewURL: 'https://cdn.pixabay.com/photo/2015/05/27/12/32/changdeokgung-786592_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/51e8d3464350b10ff3d8992cc620317f1437dbec4e507749762678d79e4cc2_640.jpg',
      webformatWidth: 640,
      webformatHeight: 425,
      largeImageURL: 'https://pixabay.com/get/51e8d3464350b108f5d0846096293f78143ed6e05a4c704f752c72d79444c45f_1280.jpg',
      imageWidth: 3008,
      imageHeight: 2000,
      imageSize: 1649085,
      views: 9872,
      downloads: 4596,
      favorites: 38,
      likes: 35,
      comments: 10,
      user_id: 211600,
      user: 'tampigns',
      userImageURL: 'https://cdn.pixabay.com/user/2015/04/16/19-49-45-34_250x250.jpg'
    },
    {
      id: 3997767,
      pageURL: 'https://pixabay.com/es/photos/arroz-arroz-blanco-corea-alimentos-3997767/',
      type: 'photo',
      tags: 'arroz, arroz blanco, corea',
      previewURL: 'https://cdn.pixabay.com/photo/2019/02/15/03/28/rice-3997767_150.jpg',
      previewWidth: 150,
      previewHeight: 84,
      webformatURL: 'https://pixabay.com/get/55e9dc444d54ab14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 359,
      largeImageURL: 'https://pixabay.com/get/55e9dc444d54ab14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 6000,
      imageHeight: 3368,
      imageSize: 3174721,
      views: 16904,
      downloads: 8641,
      favorites: 40,
      likes: 43,
      comments: 19,
      user_id: 11136103,
      user: 'allybally4b',
      userImageURL: 'https://cdn.pixabay.com/user/2019/02/18/03-58-55-979_250x250.jpg'
    },
    {
      id: 2491989,
      pageURL: 'https://pixabay.com/es/photos/mundo-la-tierra-planeta-2491989/',
      type: 'photo',
      tags: 'mundo, la tierra, planeta',
      previewURL: 'https://cdn.pixabay.com/photo/2017/07/10/23/35/globe-2491989_150.jpg',
      previewWidth: 150,
      previewHeight: 75,
      webformatURL: 'https://pixabay.com/get/54e4dc42435aa514f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 320,
      largeImageURL: 'https://pixabay.com/get/54e4dc42435aa514f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 3000,
      imageHeight: 1500,
      imageSize: 560101,
      views: 36040,
      downloads: 19747,
      favorites: 143,
      likes: 125,
      comments: 28,
      user_id: 1962238,
      user: 'qimono',
      userImageURL: 'https://cdn.pixabay.com/user/2016/03/14/20-43-09-565_250x250.png'
    },
    {
      id: 1085846,
      pageURL: 'https://pixabay.com/es/photos/noche-ciudad-ciudad-de-noche-1085846/',
      type: 'photo',
      tags: 'noche, ciudad, ciudad de noche',
      previewURL: 'https://cdn.pixabay.com/photo/2015/12/10/01/55/night-1085846_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/57e0dd464256aa14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/57e0dd464256aa14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 5184,
      imageHeight: 3456,
      imageSize: 5901573,
      views: 15148,
      downloads: 5016,
      favorites: 34,
      likes: 31,
      comments: 8,
      user_id: 1033946,
      user: 'cyh765077070',
      userImageURL: ''
    },
    {
      id: 2639315,
      pageURL: 'https://pixabay.com/es/photos/julia-roberts-2639315/',
      type: 'photo',
      tags: 'julia roberts, la bandera de corea del sur, corea del',
      previewURL: 'https://cdn.pixabay.com/photo/2017/08/14/04/20/julia-roberts-2639315_150.jpg',
      previewWidth: 150,
      previewHeight: 100,
      webformatURL: 'https://pixabay.com/get/54e6d64a4953a914f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 427,
      largeImageURL: 'https://pixabay.com/get/54e6d64a4953a914f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 4086,
      imageHeight: 2729,
      imageSize: 2258136,
      views: 11871,
      downloads: 5668,
      favorites: 18,
      likes: 37,
      comments: 7,
      user_id: 5883177,
      user: 'Big_Heart',
      userImageURL: 'https://cdn.pixabay.com/user/2017/08/11/04-18-28-984_250x250.jpg'
    },
    {
      id: 3578206,
      pageURL: 'https://pixabay.com/es/photos/corea-molino-de-viento-sal-granja-3578206/',
      type: 'photo',
      tags: 'corea, molino de viento, sal',
      previewURL: 'https://cdn.pixabay.com/photo/2018/08/01/21/24/korea-3578206_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/55e5d24b4852aa14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/55e5d24b4852aa14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 5399,
      imageHeight: 3599,
      imageSize: 7389849,
      views: 3979,
      downloads: 1754,
      favorites: 27,
      likes: 37,
      comments: 9,
      user_id: 9431329,
      user: 'cadop',
      userImageURL: ''
    },
    {
      id: 1621644,
      pageURL: 'https://pixabay.com/es/photos/rep%C3%BAblica-de-corea-racing-anapji-1621644/',
      type: 'photo',
      tags: 'república de corea, racing, anapji',
      previewURL: 'https://cdn.pixabay.com/photo/2016/08/26/09/25/republic-of-korea-1621644_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/57e6d7424c56a814f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/57e6d7424c56a814f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 5480,
      imageHeight: 3653,
      imageSize: 4987404,
      views: 7502,
      downloads: 3228,
      favorites: 29,
      likes: 32,
      comments: 7,
      user_id: 1824103,
      user: 'april_kim',
      userImageURL: 'https://cdn.pixabay.com/user/2016/06/02/07-53-44-946_250x250.png'
    },
    {
      id: 1950544,
      pageURL: 'https://pixabay.com/es/photos/el-paisaje-viajes-puesta-de-sol-1950544/',
      type: 'photo',
      tags: 'el paisaje, viajes, puesta de sol',
      previewURL: 'https://cdn.pixabay.com/photo/2017/01/03/20/25/the-landscape-1950544_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/57e9d0434f56a814f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/57e9d0434f56a814f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 2048,
      imageHeight: 1365,
      imageSize: 804674,
      views: 9459,
      downloads: 4557,
      favorites: 31,
      likes: 36,
      comments: 12,
      user_id: 4175582,
      user: 'Hachiyuuki',
      userImageURL: 'https://cdn.pixabay.com/user/2017/01/03/20-22-31-61_250x250.png'
    },
    {
      id: 3537712,
      pageURL: 'https://pixabay.com/es/photos/metro-aut%C3%B3nomo-larga-exposici%C3%B3n-3537712/',
      type: 'photo',
      tags: 'metro, autónomo, larga exposición',
      previewURL: 'https://cdn.pixabay.com/photo/2018/07/14/14/55/subway-3537712_150.jpg',
      previewWidth: 150,
      previewHeight: 99,
      webformatURL: 'https://pixabay.com/get/55e5d6444d53ae14f1dc846096293f78143ed6e05a4c704f752c72d79444c45f_640.jpg',
      webformatWidth: 640,
      webformatHeight: 426,
      largeImageURL: 'https://pixabay.com/get/55e5d6444d53ae14f6da8c7dda793676133edfed56586c48732f78dd944ecc59bf_1280.jpg',
      imageWidth: 4722,
      imageHeight: 3148,
      imageSize: 772436,
      views: 3089,
      downloads: 1593,
      favorites: 24,
      likes: 18,
      comments: 7,
      user_id: 9431329,
      user: 'cadop',
      userImageURL: ''
    }
  ]
}

export default PhotoListMock
