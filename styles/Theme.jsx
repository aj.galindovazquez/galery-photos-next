const Theme = {
    color: {
      // Brand colors
      primary: 'rgb(1, 46, 103)',
      secondary: 'rgb(195, 218, 234)',
      brand: 'rgb(229, 0, 80)',
      // Background
      backgroundPage: 'rgb(255, 250, 250)',
      // Text
      text: 'rgb(34, 34, 34)',
      // General colors
      nevada: 'rgb(109, 114, 120)',
      roseWhite: 'rgb(255, 250, 250)',
      grey: 'rgb(204, 205, 204)',
      mignightBlue: 'rgb(1, 46, 103)',
      spindle: 'rgb(195, 218, 234)',
      lightGrey: 'rgb(237, 237, 237)',
      midGrey: 'rgb(153, 153, 153)',
      darkGrey: 'rgb(68, 68, 68)'
    }
  
  }
  
  export default Theme
  