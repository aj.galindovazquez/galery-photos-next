import React from 'react'
import { PhotoCard } from '../../components/PhotoCard'
import { shallow } from 'enzyme'
import ProviderMock from '../../__mocks__/ProviderMock'

describe('<PhotoCard/>', () => {
  test('Render del componente PhotoCard', () => {
    const photoCard = shallow(
      <ProviderMock>
        <PhotoCard />
      </ProviderMock>
    )
    expect(photoCard.length).toEqual(1)
  })
  /* test('Comprobar Imagen', () => {
    const photoCard = mount(
      <ProviderMock>
        <PhotoCard />
      </ProviderMock>
    )
    expect(photoCard.find('#CardImg').length).toEqual(1)
  })
  test('Comprobar Titulo', () => {
    const photoCard = mount(
      <ProviderMock>
        <PhotoCard />
      </ProviderMock>
    )
    expect(photoCard.find('#CardTitle').length).toEqual(2)
  }) */
})
