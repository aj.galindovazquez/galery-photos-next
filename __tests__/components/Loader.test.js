import React from 'react'
import { mount } from 'enzyme'
import { Loader } from '../../components/Loader'
describe('<Loader />', () => {
  const loader = mount(<Loader />)
  test('Render del componente Loader', () => {
    expect(loader.length).toEqual(1)
  })
})
