import React from 'react'
import { mount } from 'enzyme'
import { Footer } from '../../components/Footer'
import ProviderMock from '../../__mocks__/ProviderMock'
describe('<Footer />', () => {
  const footer = mount(
    <ProviderMock>
      <Footer />
    </ProviderMock>
  )
  test('Render del componente Footer', () => {
    expect(footer.length).toEqual(1)
  })
  test('Comprobar que existe la frase del Footer', () => {
    expect(footer.find('#phrase').length).toEqual(2)
  })
})
