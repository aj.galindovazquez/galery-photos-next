import React from 'react'
import { shallow } from 'enzyme'
import ProviderMock from '../../__mocks__/ProviderMock'
import { PhotoCardList } from '../../components/PhotoCardList'
describe('<PhotoCardList/>', () => {
  test('Render del componente PhotoCardList', () => {
    const photoCardList = shallow(
      <ProviderMock>
        <PhotoCardList />
      </ProviderMock>
    )
    expect(photoCardList.length).toEqual(1)
  })
})
