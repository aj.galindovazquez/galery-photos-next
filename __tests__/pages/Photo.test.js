import React from 'react'
import { shallow } from 'enzyme'
import ProviderMock from '../../__mocks__/ProviderMock'
import Photo from '../../pages/photo/[id]'
describe('PAGE <Photo/>', () => {
  test('Render del componente Photo', () => {
    const photo = shallow(
      <ProviderMock>
        <Photo />
      </ProviderMock>
    )
    expect(photo.length).toEqual(1)
  })
})
