import React from 'react'
import { shallow } from 'enzyme'
import ProviderMock from '../../__mocks__/ProviderMock'
import Home from '../../pages/index'
describe('PAGE <Home/>', () => {
  test('Render del componente Home', () => {
    const home = shallow(
      <ProviderMock>
        <Home />
      </ProviderMock>
    )
    expect(home.length).toEqual(1)
  })
})
