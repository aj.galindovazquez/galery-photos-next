import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render () {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
          <noscript>
            <h3>Esta app necesita JavaScript para funcionar</h3>
          </noscript>
        </body>
      </Html>
    )
  }
}

export default MyDocument
