import React, { useEffect, useState } from 'react'
import {useRouter} from 'next/router'
import { PhotoView } from '../../components/PhotoView'
import { Layout } from '../../components/Layout'
import { useGetData } from '../../hooks/useGetData'
import { images } from '../../services/images'
import {GetStaticProps} from 'next'
import { report } from 'process'
import { getDiffieHellman } from 'crypto'



function getId(data) {
  const photos = data.hits
  const paths = photos.map((photo)=>({
    params:{
      id:photo.id.toString()
    }
  }))
  return paths; 
}

export const getStaticPaths = async(params)=>{
  const response= await fetch(process.env.BASE_URL_APY)
  const data = await response.json()
  const path = await getId(data)
  
  return {
   paths: [],
    //incremental static generation
    //404 for everything else
    fallback:true

  }
}

export const getStaticProps = async({params})=>{
  const id =params?.id
  console.log('id', id )
  const response = await fetch(`${process.env.BASE_URL_APY}&id=${id}`)
  const data = await response.json()
  return {
    props:{
      data
    }
  }
}

const Photo = ({data}) => {
  //const {query}= useRouter()
  // const [data, loading, error] = useGetData(images.element(query.id), false)
  const router = useRouter()

  // If the page is not yet generated, this will be displayed
  // initially until getStaticProps() finishes running
  if (router.isFallback ) {
    return <div>Loading...</div>
  }

  if(!data){
    return <div>LoadingDate...</div>
  }

  return (
    <Layout dataphoto={data.hits || null}>
      <PhotoView  image={data.hits || null} />
    </Layout>
  )
}
export default Photo