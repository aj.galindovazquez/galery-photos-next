import React, { useEffect, useState } from 'react'
import { Events, scrollSpy } from 'react-scroll'
import regeneratorRuntime from 'regenerator-runtime'
import { useGetData } from '../hooks/useGetData'
import { images } from '../services/images'
import { Header } from '../components/Header'
import { PhotoCardList } from '../components/PhotoCardList'
import { Search } from '../components/Search'
import { Layout } from '../components/Layout'

const Home = () => {
  const [enpoint, setEnpoint] = useState('')
  const [addData, setAddData] = useState(false)
  const [data, loading, error] = useGetData(enpoint, addData)
  const [page, setPage] = useState(1)
  const [header, setHeader] = useState({ q: '', image_type: 'all' })
  const [search, setSearch] = useState({ order: 'popular', orientation: 'all', category: null })
  const [isBottom, setIsBottom] = useState(false)
  //* * Scroll funtions  */
  function handleScroll () {
    const scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop
    const scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight
    if (scrollTop + window.innerHeight + 150 >= scrollHeight) { setIsBottom(true) }
  }
  useEffect(function () {
    window.addEventListener('scroll', handleScroll)
    return () => window.removeEventListener('scroll', handleScroll)
  }, [])

  // Pagination, new photos
  useEffect(function () {
    if (isBottom) {
      if (page * 50 < data.total) {
        setAddData(true)
        setPage(page + 1)
      }
    }
  }, [isBottom])

  useEffect(function () {
    setIsBottom(false)
    setEnpoint(images.list(header, search, page))
  }, [page])

  useEffect(function () {
    setAddData(false)
    setEnpoint(images.list(header, search, page))
    setPage(1)
  }, [header, search])

  function handleClick (e) {
    e.preventDefault()
    setEnpoint(images.list(header, search))
  }
  function handleChangue (e) {
    setHeader({ ...header, [e.target.name]: e.target.value })
  }
  function handleChangueSearch (e) {
    setSearch({ ...search, [e.target.name]: e.target.value })
  }
  return (
    <>
      <Layout title='Moments' subtitle='Menú principal' previewPhoto='https://static.wikia.nocookie.net/drama/images/f/fc/Start-Up-tvN-2020-02.jpg/revision/latest/scale-to-width-down/340?cb=20200911080002&path-prefix=es'>
        <Header
          onChange={handleChangue}
          onClick={handleClick}
          formValues={header}
        />
        <Search
          onChange={handleChangueSearch}
          onClick={handleClick}
          searchValues={search}
        />
        <PhotoCardList images={data.hits} total={data.total} loading={loading} />

      </Layout>
    </>
  )
}

export default Home
