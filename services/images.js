export const images = {
    element (id) {
      return (`&id=${id}`)
    },
    list (header, search, page) {
      this.paramas = `&page=${page}`
      Object.keys(header).map((key) => {
        this.paramas += `&${key}=${header[key]}`
      })
      Object.keys(search).map((key) => {
        this.paramas += `&${key}=${search[key]}`
      })
      return (this.paramas)
    }
  }
  